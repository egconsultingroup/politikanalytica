# SECRETS FOR EACH API SHOULD BE REPLACED HERE AT RUNTIME #


def twitter_config():
    return {
        "consumer_key": "replace with key",
        "consumer_secret": "replace with secret"
    }


def facebook_config():
    return {
        "consumer_key": "replace with key",
        "consumer_secret": "replace with secret"
    }


def google_trends_config():
    return {
        "consumer_key": "replace with key",
        "consumer_secret": "replace with secret"
    }

# QUERY FOR EACH API SHOULD BE REPLACED HERE AT RUNTIME #


def twitter_query():
    return "query goes here"


def facebook_query():
    return "query goes here"


def google_trends_query():
    return [["testquery1"], ["testquery2"]]

# ELASTIC CLUSTER INFO #


def es_cluster():
    return {
        "hosts": ['host1.com'],
        "username": 'xxxx',
        "password": 'yyyy',
        "port": 9200
    }
