import elasticsearch


def start(es_config):
    try:
        es = elasticsearch.Elasticsearch(
            es_config['hosts'],
            timeout=30,
            http_auth=(es_config['username'], es_config['password']),
            port=es_config['port'],
        )
        print("Connected", es.info())
        return es
    except Exception as ex:
        print("Error:", ex)
