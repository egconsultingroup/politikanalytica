from pytrends.request import TrendReq
import models.datasources.basefetcher as basefetcher


class GoogleTrendsFetcher(basefetcher.BaseFetcher):
    def __init__(self, es, config):
        basefetcher.BaseFetcher.__init__(self, es, config)
        self.trendreq = self.authenticate()

    def authenticate(self):
        return TrendReq(hl='en-US', tz=360)

    def fetch_and_persist(self, query):
        data_batch = list()
        for kw_list in query:
            pytrends = self.trendreq.build_payload(kw_list=kw_list, timeframe='now 1-d')
            data_batch.append(self.process_interest_over_time(pytrends))
            data_batch.append(self.process_interest_by_region(pytrends))
            data_batch.append(self.process_related(pytrends))

    def process_interest_over_time(self, pytrends):
        df = pytrends.interest_over_time()
        print("processing interest over time")
        return "interest over time val"

    def process_interest_by_region(self, pytrends):
        city_df = pytrends.interest_by_region(resolution='CITY')        # city level data
        country_df = pytrends.interest_by_region(resolution='COUNTRY')  # country level data
        dma_df = pytrends.interest_by_region(resolution='DMA')          # metro level data
        print("processing interest by region")
        return "interest by region val"

    def process_related(self, pytrends):
        queries_df = pytrends.related_queries()
        topics_df = pytrends.related_topics()
        print("processing related")
        return "related val"

