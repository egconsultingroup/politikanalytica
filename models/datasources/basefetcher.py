import json


class BaseFetcher:
    def __init__(self, es, config):
        self.es = es
        self.config = config

    def fetch_and_persist(self, query):
        raise Exception("fetch and persist not implemented for" + str(self.__class__))

    def authenticate(self):
        raise Exception("authenticate not implemented for" + str(self.__class__))

    def process_data(self, tweet):
        raise Exception("process data not implemented for" + str(self.__class__))

    def ingest_batch(self, data):
        """ data should be provided as a list containing the following
        object structure, this function is used to ingest data into backing
        persistent store, object should be a json string

        cleaned_data {
            datetime        : "YYYY-MM-DD HH:mm:ss",
            body            : "this is your content",
            source          : "twitter"
        }"""
        for element in data:
          self.es.index(index='twitter', doc_type='post', body=json.dumps(element))

    def ingest_metadata(self, metadata):
        """ metadata should be provided as an object with the following
        structure, this function is used to ingest metadata about a daily
        consumed dataset

        data_obj {
            date            : "YYYY-MM-DD"
            msg_count       : 12451
            source_specific : "content specific to api"
        }"""
        print(json.dumps(metadata, indent=4))
