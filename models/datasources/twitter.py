import tweepy
import datetime
import re
import string
import json
import models.datasources.basefetcher as basefetcher


class TwitterFetcher(basefetcher.BaseFetcher):
    def __init__(self, es, config):
        basefetcher.BaseFetcher.__init__(self, es, config)
        self.api = self.authenticate()

    def authenticate(self):
        """ authenticate against twitter and return back api object """

        properties = self.config
        consumer_key = properties.get("consumer_key")
        consumer_secret = properties.get("consumer_secret")

        auth = tweepy.AppAuthHandler(consumer_key, consumer_secret)
        return tweepy.API(auth, wait_on_rate_limit=True, wait_on_rate_limit_notify=True)

    def fetch_and_persist(self, query):
        """ function is used to query twitter for a given subject and get back
         all tweets, data is then cleaned into common format and ingested into
         data store to then utilize"""

        end_date = datetime.datetime.today().strftime('%Y-%m-%d')
        start_date = (datetime.datetime.today() - datetime.timedelta(days=1)).strftime('%Y-%m-%d')

        tweet_count = 0
        retweet_count = 0
        data_batch = list()

        for tweet in tweepy.Cursor(self.api.search,
                                   q=query, lang="en", since=start_date, until=end_date).items():
            tweet_count += 1
            if hasattr(tweet, 'retweeted_status'):
                retweet_count += 1

            data_batch.append(self.process_data(tweet))

            if len(data_batch) == 500:
                self.ingest_batch(data_batch)
                data_batch = list()
        self.ingest_batch(data_batch)

        metadata = {
            "datetime": str(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")),
            "msg_count": tweet_count,
            "source_specific": {
                "retweet_count": retweet_count
            }
        }
        self.ingest_metadata(metadata)

    def process_data(self, tweet):
        """ function is used to clean data into format ingestor expects before ingesting"""

        # stopwords = ["Corey Stewart", "Virginia", "Republican", "GOP", "Democrat", 2018, "Primary", " Corey",
        # "Stewart", "coreystewartva", "stewart", "corey"]
        # body = re.sub(r"http\S+", "", tweet.text)                             # remove urls
        # body = ''.join(filter(lambda x: x in set(string.printable), body))    # keeps only string.printable characters
        # body = ' '.join(filter(lambda x: x not in stopwords, body.split()))   # remove stopwords from tweet

        return {
            "body": tweet._json,
            "datetime": str(tweet.created_at),
            "source": "twitter"
        }
