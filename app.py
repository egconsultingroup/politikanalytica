from flask import Flask
import instance.config as config
import models.core.elastic_util as elastic_util
import models.datasources.twitter as twitter

app = Flask(__name__)


@app.route('/')
def hello_world():
    return 'Hello World!'


if __name__ == '__main__':
    es = elastic_util.start(config.es_cluster())
    twitter_runner = twitter.TwitterFetcher(es, config.twitter_config())
    twitter_runner.fetch_and_persist(config.twitter_query())
    # app.run()
